import { Component } from "react";
import "./App.css";
import validator from "validator";
import CreateUserModal from "./components/CreateUserModal";
import DeleteUserModal from "./components/DeleteUserModal";
import EditUserModal from "./components/EditUserModal";
import ErrorPage from "./components/ErrorPage";
import SpinnerPage from "./components/SpinnerPage";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: "",
      userIdToBeDeleted: "",
      userIdToBeEdited: "",
      isDataLoading: true,
      isEditFormSubmitted: false,
      isCreateUserFormSubmitted: false,
      isuserDeleted: false,
      defaultName: "",
      defaultEmail: "",
      userName: "",
      userEmail: "",
      createNameError: "",
      createEmailError: "",
      editNameError: "",
      editEmailError: "",
      changedName: "",
      changedEmail: "",
      defaultDownloadOption: "pdf",
      preferredDownloadOption: "",
      isServerError: false,
    };
  }

  componentDidMount() {
    fetch("http://localhost:4000/api/v1/users")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.success === false) {
          this.setState({
            isDataLoading: false,
            isServerError: true,
          });
        } else {
          this.setState({
            isServerError: false,
            data: data,
            isDataLoading: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  //Below Functions are part of CreateUserModal Component
  sendPropsToCreateUserModal = () => {
    return {
      isCreateUserFormSubmitted: this.state.isCreateUserFormSubmitted,
      userName: this.state.userName,
      userEmail: this.state.userEmail,
      createNameError: this.state.createNameError,
      createEmailError: this.state.createEmailError,
      createUserFormSubmission: this.createUserFormSubmission,
      errorMessagesCreateUserForm: this.errorMessagesCreateUserForm,
      closeCreateUserForm: this.closeCreateUserForm,
      onChangeErrorMessagesCreateUser: this.onChangeErrorMessagesCreateUser,
    };
  };

  createUserFormSubmission = (returnMessage, data) => {
    if (returnMessage === "error") {
      this.setState({
        isServerError: true,
      });
    } else {
      this.setState({
        isServerError: false,
        isDataLoading: false,
        createNameError: "",
        createEmailError: "",
        userName: "",
        userEmail: "",
        isCreateUserFormSubmitted: true,
        data: data,
      });
    }
  };

  errorMessagesCreateUserForm = () => {
    this.setState({
      createNameError:
        "Please Enter a Valid Name. Name Should Contain only letters",
      createEmailError: "Please Enter a Valid Email",
    });
  };

  onChangeErrorMessagesCreateUser = (target, event) => {
    if (target === "name") {
      if (!validator.isAlpha(event.target.value)) {
        this.setState({
          userName: event.target.value,
          createNameError:
            "Please Enter a Valid Name. Name Should Contain only letters.",
        });
      } else {
        this.setState({
          userName: event.target.value,
          createNameError: "",
        });
      }
    } else {
      if (!validator.isEmail(event.target.value)) {
        this.setState({
          userEmail: event.target.value,
          createEmailError: "Please Enter a Valid Email",
        });
      } else {
        this.setState({
          userEmail: event.target.value,
          createEmailError: "",
        });
      }
    }
  };

  closeCreateUserForm = () => {
    this.setState({
      isCreateUserFormSubmitted: false,
    });
  };

  //Getting Id of the data to be edited
  getUserIdToEdit = (event) => {
    let requiredUser = this.state.data.filter(
      (object) => object._id.toString() === event.target.id
    );
    this.setState({
      userIdToBeEdited: event.target.id,
      defaultName: requiredUser[0]["name"],
      defaultEmail: requiredUser[0]["email"],
    });
  };

  //Below Functions are part of EditUserModal Component
  sendPropsToEditUserModal = () => {
    return {
      idOfUser: this.state.userIdToBeEdited,
      isEditFormSubmitted: this.state.isEditFormSubmitted,
      defaultName: this.state.defaultName,
      defaultEmail: this.state.defaultEmail,
      changedName: this.state.changedName,
      changedEmail: this.state.changedEmail,
      editNameError: this.state.editNameError,
      editEmailError: this.state.editEmailError,
      editFormSubmissionStatus: this.editFormSubmissionStatus,
      editExistingUserDetails: this.editExistingUserDetails,
      editFormSubmissionErrorMessages: this.editFormSubmissionErrorMessages,
      editFormSubmissionChanges: this.editFormSubmissionChanges,
    };
  };

  editFormSubmissionStatus = () => {
    this.setState({
      isEditFormSubmitted: false,
    });
  };

  editExistingUserDetails = (target, event) => {
    if (target === "name") {
      if (!validator.isAlpha(event.target.value)) {
        this.setState({
          editNameError:
            "Please Enter a Valid Name. Name Should Contain only letters",
          changedName: event.target.value,
        });
      } else {
        this.setState({
          editNameError: "",
          changedName: event.target.value,
        });
      }
    } else {
      if (!validator.isEmail(event.target.value)) {
        this.setState({
          editEmailError: "Please Enter a Valid Email Address",
          changedEmail: event.target.value,
        });
      } else {
        this.setState({
          editEmailError: "",
          changedEmail: event.target.value,
        });
      }
    }
  };

  editFormSubmissionErrorMessages = () => {
    if (!validator.isAlpha(this.state.changedName) && !this.state.defaultName) {
      this.setState({
        editNameError: "Please Enter a Valid Name",
      });
    }

    if (
      !validator.isEmail(this.state.changedEmail) &&
      !this.state.defaultEmail
    ) {
      this.setState({
        editEmailError: "Please Enter a Valid Email Address",
      });
    }
  };

  editFormSubmissionChanges = (returnMessage, data) => {
    if (returnMessage === "error") {
      this.setState({
        isServerError: true,
      });
    } else {
      this.setState({
        isServerError: false,
        userIdToBeEdited: "",
        isEditFormSubmitted: true,
        defaultName: "",
        defaultEmail: "",
        changedName: "",
        changedEmail: "",
        data: data,
      });
    }
  };

  //Getting Id of the user to be Deleted
  getUserIdToDelete = (event) => {
    this.setState({
      userIdToBeDeleted: event.target.id,
    });
  };

  //Below Functions are part of DeleteUserModal Component
  sendPropsToDeleteUserModal = () => {
    return {
      idOfUser: this.state.userIdToBeDeleted,
      deleteUserModalControl: this.state.isuserDeleted,
      userDataDeletionStatus: this.userDataDeletionStatus,
      userDeleteCloseModal: this.userDeleteCloseModal,
    };
  };

  userDataDeletionStatus = (returnMessage, data) => {
    if (returnMessage === "error") {
      this.setState({
        isServerError: true,
      });
    } else {
      this.setState({
        isServerError: false,
        userIdToBeDeleted: "",
        data: data,
        isuserDeleted: true,
      });
    }
  };

  userDeleteCloseModal = () => {
    this.setState({
      isuserDeleted: false,
    });
  };

  handleDataView = (stateData) => {
    //filtering the data and handling the view of the user info based on isDeleted Property
    let dataTobeRendered = stateData.filter(
      (object) => object.isDeleted === false
    );
    let containerView = dataTobeRendered.map((object, index) => {
      return (
        <tr key={index}>
          <td>{object.name}</td>
          <td>
            {object.email}
            {object.isActive ? (
              <i
                className="fa-solid fa-circle-check text-success verification-icons"
                title="verified"
              ></i>
            ) : (
              <i
                className="fa-solid fa-circle-exclamation text-danger verification-icons"
                title="Pending Verification. Account Verification mail sent to registered email address"
              ></i>
            )}
          </td>
          <td>{JSON.stringify(object.isActive)}</td>
          <td className="text-center">
            <i
              className="fa-solid fa-pen-to-square edit-delete"
              id={object._id}
              onClick={this.getUserIdToEdit}
              data-bs-toggle="modal"
              data-bs-target="#editConfirmation"
            ></i>
          </td>
          <td className="text-center">
            <i
              className="fa-solid fa-trash edit-delete"
              id={object._id}
              onClick={this.getUserIdToDelete}
              data-bs-toggle="modal"
              data-bs-target="#deleteConfirmation"
            ></i>
          </td>
        </tr>
      );
    });

    return containerView;
  };

  handleSelectDownloads = (event) => {
    event.preventDefault();

    if (event.target.value.endsWith("PDF")) {
      this.setState({
        preferredDownloadOption: "pdf",
      });
    } else {
      this.setState({
        preferredDownloadOption: "csv",
      });
    }
  };

  render() {
    //Spinner Code
    if (this.state.isDataLoading) {
      return <SpinnerPage />;
    }
    //Server Error Code
    else if (this.state.isServerError) {
      return <ErrorPage />;
    }
    //Code renders after data is successfully fetched from db
    else if (this.state.data) {
      return (
        <>
          <div className="container">
            <div className="col-12 mt-5">
              <h3>RADIARE ADMIN PORTAL</h3>
              <hr />
              <div className="d-flex justify-content-between">
                <button
                  className="btn btn-secondary mb-5 mt-3"
                  data-bs-toggle="modal"
                  data-bs-target="#createConfirmation"
                >
                  CREATE USER
                </button>
                <div>
                  <select
                    className="text-secondary"
                    defaultValue={this.state.defaultDownloadOption}
                    onChange={this.handleSelectDownloads}
                  >
                    <option>Download as PDF</option>
                    <option>Download as CSV</option>
                  </select>
                  <a
                    href={
                      this.state.preferredDownloadOption
                        ? `http://localhost:4000/api/v1/users/download.${this.state.preferredDownloadOption}`
                        : `http://localhost:4000/api/v1/users/download.${this.state.defaultDownloadOption}`
                    }
                  >
                    <button type="button" className="btn btn-secondary">
                      DOWNLOAD USER DATA
                    </button>
                  </a>
                </div>
              </div>
            </div>
            <div className="row content-container p-5 mb-5">
              <h5 style={{ textDecoration: "underline" }}>USERS LIST: </h5>
              <table id="usersDataTable">
                <tbody>
                  <tr>
                    <th>NAME</th>
                    <th>EMAIL ADDRESS</th>
                    <th>ACTIVE</th>
                    <th>EDIT</th>
                    <th>DELETE</th>
                  </tr>
                  {this.handleDataView(this.state.data)}
                </tbody>
              </table>
            </div>
          </div>

          {/* Create User Modal */}
          <CreateUserModal propsObject={this.sendPropsToCreateUserModal()} />

          {/* Delete Confirmation */}
          <DeleteUserModal propsObject={this.sendPropsToDeleteUserModal()} />

          {/* Edit Confirmation */}
          <EditUserModal propsObject={this.sendPropsToEditUserModal()} />
        </>
      );
    }
  }
}

export default App;
