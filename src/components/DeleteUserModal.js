import React, { Component } from "react";

class DeleteUserModal extends Component {
  constructor(props) {
    super(props);
  }

  //Soft Deleting the user, only from UI not from DB
  handleDelete = () => {
    let id = this.props.propsObject.idOfUser;

    fetch(`http://localhost:4000/api/v1/users/${id}`, { method: "DELETE" })
      .then(() => {
        fetch("http://localhost:4000/api/v1/users/")
          .then((response) => response.json())
          .then((data) => {
            if (data.success === false) {
              this.props.propsObject.userDataDeletionStatus("error", null);
            } else {
              this.props.propsObject.userDataDeletionStatus("noError", data);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleDeleteModal = () => {
    this.props.propsObject.userDeleteCloseModal();
  };

  render() {
    return (
      <div
        className="modal fade"
        id="deleteConfirmation"
        data-bs-backdrop="false"
        tabIndex="-1"
        aria-labelledby="deleteConfirmationLabel"
        aria-hidden="true"
      >
        {/* data-bs-backdrop="false" Added property to remove black screen after modal activity finished*/}
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              {this.props.propsObject.deleteUserModalControl ? (
                <div>
                  <div className="modal-header">
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      onClick={this.handleDeleteModal}
                    ></button>
                  </div>
                  <h5 className="text-success text-center m-5">
                    User Deleted Successfully
                  </h5>
                </div>
              ) : (
                <div>
                  <h3>Are you sure, you want to delete the Product?</h3>
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-secondary"
                      data-bs-dismiss="modal"
                    >
                      No
                    </button>
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={this.handleDelete}
                    >
                      Yes
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DeleteUserModal;
