import React from "react";

function ErrorPage() {
  return (
    <>
      <div
        style={{ height: "100vh" }}
        className="d-flex justify-content-center align-items-center"
      >
        <div className="text-center">
          <h6>Code: 500</h6>
          <p>
            Oops! Looks like something went wrong, Please try Again after
            sometime!
          </p>
        </div>
      </div>
    </>
  );
}

export default ErrorPage;

