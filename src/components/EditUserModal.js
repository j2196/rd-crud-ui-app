import React, { Component } from "react";

class EditUserModal extends Component {
  constructor(props) {
    super(props);
  }

  //Handling change events of the user editings
  handleUserInfoEdits = (event) => {
    if (event.target.name === "name") {
      this.props.propsObject.editExistingUserDetails("name", event);
    } else {
      this.props.propsObject.editExistingUserDetails("email", event);
    }
  };

  //Sending edited data in body and rendering the data
  handleUserEdits = (event) => {
    event.preventDefault();
    this.props.propsObject.editFormSubmissionErrorMessages();
    let id = this.props.propsObject.idOfUser;

    if (
      !this.props.propsObject.editNameError &&
      !this.props.propsObject.editEmailError &&
      ((this.props.propsObject.changedEmail &&
        this.props.propsObject.changedName) ||
        (this.props.propsObject.defaultName &&
          this.props.propsObject.defaultEmail))
    ) {
      fetch(`http://localhost:4000/api/v1/users/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: this.props.propsObject.changedName,
          email: this.props.propsObject.changedEmail,
        }),
      })
        .then(() => {
          fetch("http://localhost:4000/api/v1/users/")
            .then((response) => response.json())
            .then((data) => {
              if (data.success === false) {
                this.props.propsObject.editFormSubmissionChanges("error", null);
              } else {
                this.props.propsObject.editFormSubmissionChanges(
                  "noError",
                  data
                );
              }
            })
            .catch((error) => {
              console.log(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  handleEditFormStatus = () => {
    this.props.propsObject.editFormSubmissionStatus();
  };

  render() {
    return (
      <div>
        <div
          className="modal fade"
          id="editConfirmation"
          data-bs-backdrop="false"
          tabIndex="-1"
          aria-labelledby="editConfirmationLabel"
          aria-hidden="true"
        >
          {/* data-bs-backdrop="false" Added property to remove black screen after modal activity finished*/}
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                {this.props.propsObject.isEditFormSubmitted ? (
                  <div>
                    <div className="modal-header">
                      <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                        onClick={this.handleEditFormStatus}
                      ></button>
                    </div>
                    <h5 className="text-success text-center m-5">
                      User Details updated successfully
                    </h5>
                  </div>
                ) : (
                  <div>
                    <div className="modal-header">
                      <button
                        type="button"
                        className="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <form onSubmit={this.handleUserEdits}>
                      <div className="d-flex flex-column mb-3">
                        <label htmlFor="userNameId">Name</label>
                        <input
                          type="text"
                          id="userNameId"
                          onChange={this.handleUserInfoEdits}
                          name="name"
                          defaultValue={this.props.propsObject.defaultName}
                        />
                        <p className="text-danger">
                          {this.props.propsObject.editNameError}
                        </p>
                      </div>
                      <div className="d-flex flex-column">
                        <label htmlFor="userEmailId">Email</label>
                        <input
                          type="text"
                          id="userEmailId"
                          onChange={this.handleUserInfoEdits}
                          name="email"
                          defaultValue={this.props.propsObject.defaultEmail}
                        />
                        <p className="text-danger">
                          {this.props.propsObject.editEmailError}
                        </p>
                      </div>

                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-secondary"
                          data-bs-dismiss="modal"
                        >
                          No
                        </button>
                        <button type="submit" className="btn btn-primary">
                          Save Changes
                        </button>
                      </div>
                    </form>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditUserModal;
