import React, { Component } from "react";

class CreateUserModal extends Component {
  constructor(props) {
    super(props);
  }

  handleCloseCreateUserForm = () => {
    this.props.propsObject.closeCreateUserForm();
  };

  handleSubmitPost = (event) => {
    event.preventDefault();

    if (this.props.propsObject.userName && this.props.propsObject.userEmail) {
      let postObject = {
        name: this.props.propsObject.userName,
        email: this.props.propsObject.userEmail,
        isDeleted: false,
        isActive: false,
      };

      fetch("http://localhost:4000/api/v1/users/", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(postObject),
      })
        .then(() => {
          fetch("http://localhost:4000/api/v1/users/")
            .then((response) => response.json())
            .then((data) => {
              if (data.success === false) {
                this.props.propsObject.createUserFormSubmission("error", null);
              } else {
                this.props.propsObject.createUserFormSubmission(
                  "NoError",
                  data
                );
              }
            })
            .catch((error) => {
              console.log(error);
            });
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      this.props.propsObject.errorMessagesCreateUserForm();
    }
  };

  handleCreateUser = (event) => {
    if (event.target.name === "name") {
      this.props.propsObject.onChangeErrorMessagesCreateUser("name", event);
    } else {
      this.props.propsObject.onChangeErrorMessagesCreateUser("email", event);
    }
  };

  render() {
    return (
      <div
        className="modal fade"
        id="createConfirmation"
        data-bs-backdrop="false"
        tabIndex="-1"
        aria-labelledby="createConfirmationLabel"
        aria-hidden="true"
      >
        {/* data-bs-backdrop="false" Added property to remove black screen after modal activity finished*/}
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-body">
              {this.props.propsObject.isCreateUserFormSubmitted ? (
                <div>
                  <div className="modal-header">
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      onClick={this.handleCloseCreateUserForm}
                      aria-label="Close"
                    ></button>
                  </div>
                  <h5 className="text-success text-center m-5">
                    User Created Successfully
                  </h5>
                </div>
              ) : (
                <div>
                  <div className="modal-header">
                    <button
                      type="button"
                      className="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                    ></button>
                  </div>
                  <form onSubmit={this.handleSubmitPost}>
                    <div className="d-flex flex-column mb-3">
                      <input
                        type="text"
                        name="name"
                        placeholder="name"
                        className="mb-2"
                        value={this.props.userName}
                        onChange={this.handleCreateUser}
                      />
                      <p className="text-danger">
                        {this.props.propsObject.createNameError}
                      </p>
                      <input
                        type="text"
                        name="email"
                        placeholder="email"
                        value={this.props.userEmail}
                        onChange={this.handleCreateUser}
                      />
                      <p className="text-danger">
                        {this.props.propsObject.createEmailError}
                      </p>
                    </div>

                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary"
                        data-bs-dismiss="modal"
                      >
                        Close
                      </button>
                      <button type="submit" className="btn btn-primary">
                        Create User
                      </button>
                    </div>
                  </form>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateUserModal;
